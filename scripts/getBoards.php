<?php
  # echo "id = $forumID";

  $server   = $GLOBALS['SQL_servername'];
  $username = $GLOBALS['SQL_username'];
  $password = $GLOBALS['SQL_password'];
  
  $connection = mysqli_connect($server, $username, $password, $username);

  if($connection)
  {
    # get forum's name from its id
    $statement = $connection->prepare("SELECT ForumName from forum where ForumID = ? limit 1;");
    if($statement)
    {
      $statement->bind_param("i", $fid);
      $fid = $forumID;
      $statement->execute();
      $statement->bind_result($fname);
      if($statement->fetch())
      {
        echo "<script>";
        echo "var title = document.title;";
        echo "document.title = title.replace(\"%forum%\",\"$fname\");";
        echo "var banner = document.getElementById(\"banner\").innerHTML;";
        echo "document.getElementById(\"banner\").innerHTML = banner.replace(\"%forum%\",\"$fname\");";
        echo "</script>";
      }
      $statement->close();
    }


    # prepare a statement
    $statement = $connection->prepare("SELECT BoardID, BoardName from board where ForumID = ?;");

    if($statement)
    {
      # I have no idea why we need both execute() and fetch()

      $statement->bind_param("i", $fid);
      $fid = $forumID;

      $statement->execute();

      $statement->bind_result($id, $name);

      echo "<div id=\"board_div\">";
      echo "<table id=\"board\" class=\"board_table_class\">";
      while($statement->fetch())
      {
        echo "<tr><td>";
        echo "<a class=\"board_link\" href=\"pages/board.php?id=$id\"> $name</a>";
        echo "</tr></td>";
      }
      echo "</table>";
      echo "</div>";

      $statement->close();
    }
    else
    {
      echo "statement failed";
    }

    $connection->close();
  }
  else
  {
    echo "connection failed";
  }
?>
