<?php
  $server   = $GLOBALS['SQL_servername'];
  $username = $GLOBALS['SQL_username'];
  $password = $GLOBALS['SQL_password'];
  
  $connection = mysqli_connect($server, $username, $password, $username);

  if($connection)
  {
    # prepare a statement
    $statement = $connection->prepare("SELECT ForumID, ForumName, Permission from forum;");

    if($statement)
    {
      # I have no idea why we need both execute() and fetch()

      $statement->execute();

      $statement->bind_result($id, $name, $prm);

      echo "<div id=\"forum_div\">";
      echo "<table id=\"forum_table\" class=\"forum_table_class\">";
      while($statement->fetch())
      {
        echo "<tr><td>";
        echo "<a class=\"forum_link\" href=\"pages/forum.php?id=$id\"> $name</a>";
        echo "</tr></td>";
      }
      echo "</table>";
      echo "</div>";

      $statement->close();
    }
    else
    {
      echo "statement failed";
    }

    $connection->close();
  }
  else
  {
    echo "connection failed";
  }

?>
